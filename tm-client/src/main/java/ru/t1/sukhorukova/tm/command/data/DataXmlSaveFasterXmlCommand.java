package ru.t1.sukhorukova.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.sukhorukova.tm.dto.request.data.DataXmlSaveFasterXmlRequest;

public final class DataXmlSaveFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-save-xml";

    @NotNull
    public static final String DESCRIPTION = "Save data in xml file.";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA SAVE XML]");

        getDomainEndpoint().xmlSaveFasterXmlData(new DataXmlSaveFasterXmlRequest(getToken()));
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
