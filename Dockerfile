FROM ubuntu:14.04
MAINTAINER Victiria Sukhorukova (vsukhorukova@t1-consulting.ru)

RUN apt-get update -y
RUN apt-get install -y apache2 mc

RUN echo 'ServerName webserver' >> /etc/apache2/apache2.conf

RUN a2enmod proxy
RUN a2enmod proxy_http
RUN a2enmod proxy_balancer
RUN a2enmod lbmethod_byrequests

RUN echo '<VirtualHost *:6060>\n\
    ProxyRequests Off \n\
    ProxyPreserveHost On \n\
    <Proxy balancer://mycluster> \n\
        BalancerMember http://alpha:6060 \n\
        BalancerMember http://betta:6060 \n\
        BalancerMember http://gamma:6060 \n\
        ProxySet lbmethod=byrequests \n\
    </Proxy> \n\
    ProxyPass / balancer://mycluster/ \n\
    ProxyPassReverse / balancer://mycluster/ \n\
</VirtualHost>\n'\
> /etc/apache2/sites-enabled/000-default.conf

RUN service apache2 restart

EXPOSE 6060
CMD ["apache2ctl", "-D", "FOREGROUND"]