package ru.t1.sukhorukova.tm.exception.user;

public final class LockedException extends AbstractUserException {

    public LockedException() {
        super("Error! User is locked...");
    }

}
