package ru.t1.sukhorukova.tm.exception.system;

import org.jetbrains.annotations.NotNull;

public final class ConnectionFailedException extends AbstractSystemException {

    public ConnectionFailedException() {
        super("Error! The connection is failed...");
    }

    public ConnectionFailedException(@NotNull final String property) {
        super("Error! Wrong value of " + property);
    }

}
